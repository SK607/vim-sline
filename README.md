# SLine
A lightweight statusline for Vim9.

![Supports: Vim9](./media/supports-badge.svg)
[![License: MIT](./media/license-badge.svg)](./LICENSE)

![Features](./media/features.png)


## Auto-theming

![Themes](./media/themes.png)

Examples: [tokyonight](https://github.com/ghjkevin/tokyonight-vim), [gruvbox](https://github.com/morhetz/gruvbox), [onedark](https://github.com/joshdick/onedark.vim), [dracula](https://github.com/dracula/vim), [catppuccin](https://github.com/catppuccin/vim).

> **Note**
> SLine tries to ajust to your theme automatically.
> But sometimes it fails due to lack of compatibility
> across Vim colorschemes.


## Installation
Using [vim-plug](https://github.com/junegunn/vim-plug)
```vim
Plug 'https://codeberg.org/SK607/vim-sline'
```


## Configuration
See `help sline`
