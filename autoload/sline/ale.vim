vim9script


export def SetALE()
    const counts = ale#statusline#Count(bufnr())
    b:sline_ale_errors = get(g:, 'ale_sign_error', 'e')
        .. ':' .. counts.error
    b:sline_ale_warnings = get(g:, 'ale_sign_warning', 'w')
        .. ':' .. counts.warning
enddef

