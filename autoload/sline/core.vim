vim9script


export def SetCore()
    if !exists('b:sline_fullpath')
        SetFullPath()
    endif
    if !exists('b:sline_git_root')
        SetGitRoot()
        if filereadable(b:sline_git_root .. '/.git/HEAD')
            SetGitBranch()
        endif
    endif
    if get(b:, 'sline_wd', '') !=# getcwd()
        SetWD()
        SetRootPath()
        SetRelativePath()
    endif
enddef


def SetFullPath()
    b:sline_fullpath = resolve(expand('%:p'))
enddef

def SetWD()
    b:sline_wd = getcwd()
enddef

def SetGitRoot()
    var path = fnamemodify(b:sline_fullpath, ':h')
    var prev_path = ''
    var git_root = ''
    while path !=# prev_path
        if filereadable(path .. '/.git/HEAD')
            git_root = path
            break
        endif
        prev_path = path
        path = fnamemodify(path, ':h')
    endwhile
    b:sline_git_root = git_root
enddef

def SetGitBranch()
    var git_branch = ''
    var ref = get(readfile(b:sline_git_root .. '/.git/HEAD'), 0, '')
    if ref =~# '^ref: '
        const rexp = '^ref: \%(refs/\%(heads/\|remotes/\|tags/\)\=\)\='
        git_branch = substitute(ref, rexp, '', '')
    elseif ref =~# '^\x\{20\}'
        # detached HEAD state
        git_branch = ref[: 6]
    endif
    b:sline_git_branch = git_branch
enddef

def SetRootPath()
    const git = b:sline_git_root
    const wd = getcwd()
    var root = ''
    if StartsWith(b:sline_fullpath, wd)
        root = fnamemodify(wd, ':t')
        if StartsWith(wd, git) && wd !=# git
            const root_git = fnamemodify(git, ':t')
            if git .. '/' .. root ==# wd
                root = root_git .. '/' .. root
            else
                root = root_git .. '/.../' .. root
            endif
        elseif wd ==# $HOME
            root = '~'
        endif
    endif
    b:sline_root = root
enddef

def SetRelativePath()
    const wd = getcwd()
    const git_root = b:sline_git_root
    const fullpath = b:sline_fullpath
    var relpath = ''
    if StartsWith(fullpath, wd)
        relpath = fullpath[len(wd) + 1 : ]
    else
        relpath = fullpath
        if StartsWith(relpath, git_root)
            const git_dir = fnamemodify(git_root, ':t')
            relpath = substitute(relpath, git_root, git_dir, '')
        elseif StartsWith(relpath, $HOME)
            relpath = substitute(relpath, $HOME, '~', '')
        endif
    endif
    b:sline_relpath = relpath
enddef


def StartsWith(str: string, substr: string): bool
    return str[0 : len(substr) - 1] ==# substr
enddef
