vim9script


def g:SLine(): string
    const bufnr = winbufnr(g:statusline_winid)
    const git = getbufvar(bufnr, 'sline_git_branch', '')
    const root = getbufvar(bufnr, 'sline_root', '')
    const errors = getbufvar(bufnr, 'sline_ale_errors', '')
    const warnings = getbufvar(bufnr, 'sline_ale_warnings', '')
    const bfiletype = getbufvar(bufnr, '&filetype')
    var line = ''
    if g:statusline_winid == win_getid()
        line ..= (tabpagenr('$') > 1) ? '%#StatusLineTabNr# ' .. tabpagenr() .. ' ' : ''
        line ..= Mode()
        line ..= '%#StatusLine#' .. ' '
        line ..= FileStatus(bufnr)
        line ..= !empty(git) ? '%#SLineGitBranch#' .. git .. ':' : ''
        line ..= !empty(root) ? '%#SLineRootDir#' .. root .. '/' : ''
        line ..= '%#StatusLine#'
        line ..= '%<' .. getbufvar(bufnr, 'sline_relpath', '')
        line ..= '%='
        if !empty(errors)
            line ..= ' '
            line ..= (errors ==# 'e:0') ? '' : '%#SLineLintErr#'
            line ..= errors .. '%#StatusLine#' .. ' '
            line ..= (warnings ==# 'w:0') ? '' : '%#SLineLintWarn#'
            line ..= warnings .. '%#StatusLine#'
        endif
    else
        line ..= '%#StatusLineNC#'
        line ..= FileStatus(bufnr)
        line ..= !empty(git) ? git .. ':' : ''
        line ..= !empty(root) ? root .. '/' : ''
        line ..= '%<' .. getbufvar(bufnr, 'sline_relpath', '')
        line ..= '%='
        if !empty(errors)
            line ..= errors .. ' ' .. warnings
        endif
    endif
    line ..= ' '
    line ..= !empty(bfiletype) ? '[' .. bfiletype .. ']' : '[-]'
    line ..= ' '
    return line
enddef


def FileStatus(bufnr: number): string
    var status = ''
    if index(['t'], mode(1)) < 0
        if getbufvar(bufnr, "&readonly")
            status ..= g:sline_readonly
        elseif getbufvar(bufnr, "&modified")
            status ..= g:sline_modified
        else
            status ..= g:sline_unmodified
        endif
        status ..= ' '
    endif
    return status
enddef

def Mode(): string
    const mode = mode(1)
    var name = ''
    var style = ''
    if index(['R', 'Rc', 'Rv', 'Rx'], mode) >= 0
        name = 'REPLACE'
        style = '%#SLineInsertMode#'
    elseif index(['i', 'ic', 'ix'], mode) >= 0
        name = 'INSERT'
        style = '%#SLineInsertMode#'
    elseif index(['v', 'V', "\<C-V>", 's', 'S', "\<C-S>"], mode) >= 0
        name = 'VISUAL'
        style = '%#SLineVisualMode#'
    elseif index(['t'], mode) >= 0
        name = 'TERM'
        style = '%#SLineNormalMode#'
    elseif index(['c', 'cv', 'ce', 'r', 'rm', 'r?'], mode) >= 0
        name = 'COMMAND'
        style = '%#SLineCommandMode#'
    else
        name = 'NORMAL'
        style = '%#SLineNormalMode#'
    endif
    return style .. printf(' %7S ', name)
enddef













