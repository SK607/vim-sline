vim9script


export def SetDefaultTheme()
    var fg = GetColor('StatusLine', 'fg')
    var bg = GetColor('StatusLine', 'bg')
    if ! (exists('&t_Co') && str2nr(&t_Co) >= 256) || ! &termguicolors
        fg = GetColor('EndOfBuffer', 'fg')
        bg = GetColor('EndOfBuffer', 'bg')
        var fg_nc = GetColor('StatusLineNC', 'fg')
        execute 'highlight! StatusLine term=NONE cterm=NONE gui=NONE ctermfg=' .. fg .. ' ctermbg=' .. bg
        execute 'highlight! StatusLineNC term=NONE cterm=NONE gui=NONE ctermfg=' .. fg_nc .. ' ctermbg=' .. bg
    endif
    if fg ==# 'NONE'
        fg = GetColor('Normal', 'fg')
    endif
    highlight! link StatusLineTerm StatusLine
    highlight! link StatusLineTermNC StatusLineNC
    SetElement('NormalMode', bg, fg)
    SetElement('InsertMode', bg, GetColor('Question', 'fg'))
    SetElement('VisualMode', bg, GetColor('Float', 'fg'))
    SetElement('CommandMode', bg, GetColor('Function', 'fg'))
    SetElement('GitBranch', GetColor('String', 'fg'), bg)
    SetElement('RootDir', GetColor('Keyword', 'fg'), bg)
    SetElement('LintErr', GetColor('ALEErrorSign', 'fg'), bg)
    SetElement('LintWarn', GetColor('ALEWarningSign', 'fg'), bg)
enddef

def SetElement(name: string, fg: string, bg: string)
    if &termguicolors
        execute 'highlight default SLine' .. name .. ' guifg=' .. fg .. ' guibg=' .. bg
    else
        execute 'highlight default SLine' .. name .. ' ctermfg=' .. fg .. ' ctermbg=' .. bg
    endif
enddef

def GetColor(hlgroup: string, attr: string): string
    const value = GetStyle(hlgroup, ResolveAttr(hlgroup, attr))
    return empty(value) ? 'NONE' : value
enddef

def ResolveAttr(hlgroup: string, name: string): string
    var value = name
    if GetStyle(hlgroup, 'reverse') == '1'
        value = (name == 'fg') ? 'bg' : 'fg'
    endif
    return value
enddef

def GetStyle(hlgroup: string, attr: string): string
    return synIDattr(synIDtrans(hlID(hlgroup)), attr)
enddef
