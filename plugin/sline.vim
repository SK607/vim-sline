if !has('vim9script') || v:version < 900
    finish
endif

vim9script

import '../autoload/sline/statusline.vim'
import '../autoload/sline/core.vim' as core
import '../autoload/sline/theme.vim' as theme
import autoload '../autoload/sline/ale.vim' as ale

g:sline_readonly = '[!]'
g:sline_modified = '[+]'
g:sline_unmodified = '[-]'

augroup SLine
    autocmd!
    autocmd BufReadPre,DirChanged,BufWritePost * :call core.SetCore()
    autocmd User ALELintPost :silent call ale.SetALE()
    autocmd VimEnter,ColorScheme * :call theme.SetDefaultTheme()
augroup END

set statusline=%!SLine()
